﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHit : MonoBehaviour
{
	//Hit points
	public int health = 2;
	public int pointsPerEnemy = 10;

	private GameController controller;

	//A reference to the Explosion prefab
	//in Object Orientation we call this an
	//association.
	public Transform explosion;

	void Start()
	{
		//We'll look up the controller based on the type 
		//we set earlier - we'll do this once and store it
		//more efficent. 
		controller = GameObject.FindObjectOfType<GameController>();
	}
		
	void OnCollisionEnter2D(Collision2D theCollision)
	{
		//Uncomment this line to output the name of colliding object
		//Debug.Log("Hit " + theCollision.gameObject.name);

		//Try to find a component called LaserMovement in the other object
		LaserMovement laser = theCollision.gameObject.GetComponent<LaserMovement>();

		if (laser != null) //check if it exists.
		{
			//Reduce my health by the damage of the laser.
			health -= laser.damage;

			//Destroy the laser
			Destroy(theCollision.gameObject);
		}
		else
		{
			/* We've hit another ship */
		}

		if(health <= 0)
		{
			if (controller != null)
			{
				//Inform the controller than the EnemyShip object
				//has been destroyed. 
				controller.killedEnemy();

				//Inform the controller of the points to 
				//be added based on the pointsPerEnemy for
				//this enemy.
				controller.increaseScore (this.pointsPerEnemy);
			}
			else
			{
				Debug.Log("No GameContoller object attached.");
			}

			if (explosion != null)
			{
				GameObject exploder = (Instantiate<Transform>(explosion, this.transform.position, this.transform.rotation)).gameObject;
				Destroy(exploder,2.0f);
			}
			else
			{
				Debug.Log("No Explosion (partical effect prefab) object attached.");
			}

			Destroy(this.gameObject);
		}
	}
}