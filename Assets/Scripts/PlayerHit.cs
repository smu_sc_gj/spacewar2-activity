﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHit : MonoBehaviour
{
	//Hit points
	public int health = 10;

	//GameController - to inform it of our death
	public GameController controller;

	void Start()
	{

	}

	void OnCollisionEnter2D(Collision2D theCollision)
	{
		//Uncomment this line to output the name of colliding object
		Debug.Log("Hit " + theCollision.gameObject.name);

		//Try to find a component called EnemyHit behaviour in the other object
		MoveTowardsPlayer enemy = theCollision.gameObject.GetComponent<MoveTowardsPlayer>();

		if (enemy != null) //check if it exists.
		{
			//Reduce my health by the damage of the laser.
			health -= enemy.collisionDamage;

			Destroy(theCollision.gameObject);
		
		}
		else
		{
			/* We've hit another player or something else */
		}

		if(health <= 0)
		{
			controller.playerDead ();
			Destroy(this.gameObject);
		}
	}
}