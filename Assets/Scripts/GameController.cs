﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Required for Text (and other UI components). 
using UnityEngine.UI;
//Required to re-load the scene (i.e. restart). 
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private Transform enemy;

    [Header("Wave Properties")]
    //Delays during spawning/wave creation
    public float timeBeforeSpawning = 1.5f;
    public float timeBetweenEnemies = 0.25f;
    public float timeBeforeWaves = 2.0f;

    public int enemiesPerWave = 10;

    //These are internal to the class
    private int currentNumberOfEnemies = 0;

	private int score = 0;
	private int waveNumber = 0;

	[Header("User Interface")]
	public Text scoreText;
	public Text waveText;

	[Header("Game Over Message")]
	public Text gameOverText;
	public Text restartMessageText;


    // Use this for initialization
    void Start ()
    {
        // Debugging - testing the
        // creation of a single enemy.
        //createOneRandomEnemy();

        // Start spawning enermies
        StartCoroutine(spawnEnemies());

		//Hide the game overText
		showGameOverText(false);
    }

	// Show / hide the game over and restart
	// message text. 
	private void showGameOverText(bool toggle)
	{
		gameOverText.enabled = toggle;
		restartMessageText.enabled = toggle;
	}

    // Update is called once per frame
    void Update ()
    {
		ScoreTextUpdate ();
		WaveTextUpdate ();

		//Check for the reset key
		//Could restrict this to only work if game is over. 
		if(Input.GetKeyDown(KeyCode.R))
		{
			//Reload the scene
			SceneManager.LoadScene("Spacewar");
		}	
    }

    IEnumerator spawnEnemies()
    {
        //Give the player time before we start spawing
        yield return new WaitForSeconds(timeBeforeSpawning);

        //When timeBeforeSpawning elapses 
        while (true)
        {
            //Don't spawn until last wave are all destroyed
            if (currentNumberOfEnemies <= 0)
            {
                //Spawn enemiesPerWave enemies in a random position
                for (int i = 0; i < enemiesPerWave; i++)
                {
                    createOneRandomEnemy();
                    currentNumberOfEnemies++;              

                    //Wait timeBetweenEnemies and resume
                    yield return new WaitForSeconds(timeBetweenEnemies);

                }
            }


            //Wait before checking if we need to send in another wave. 
            //Must yeild (give up the cpu,) else the tight loop will 'crash'
            //unity. 
            yield return new WaitForSeconds(timeBeforeWaves);
        }
    }

    // Private / Helper Method
    // Creates one single enemy at a random position on screen. 
    private void createOneRandomEnemy()
    {
        //Start them off screen 

        // We want the enemies to be off screen
        // (Random.Range gives us a number between the
        // first and second parameter)
        float randDistance = Random.Range(25, 50);

        //Enemies can come from any direction
        Vector2 randDirection = Random.insideUnitCircle;
        Vector3 enemyPos = this.transform.position;

        //Use distance and drection to set new position
        enemyPos.x += randDirection.x * randDistance;
        enemyPos.y += randDirection.y * randDistance;

        //Spawn and enemy
        Instantiate(enemy, enemyPos, this.transform.rotation);
    }

    //Allow outside classes to inform gamecontroller of an 
    //enemy destroyed. 
    public void killedEnemy()
    {
        currentNumberOfEnemies--;
    }

	//Allow outside classes to inform GameController
	//of points to be awarded
	public void increaseScore(int points)
	{
		score += points;

		//For debugging
		Debug.Log("Current score: " + score);
	}

	private void ScoreTextUpdate()
	{
		if (scoreText != null)
			scoreText.text = "Score: " + score;
	}

	private void WaveTextUpdate()
	{
		if (waveText != null)
			waveText.text = "Wave: " + waveNumber;
	}

	public void playerDead()
	{
		//Set the game over text to visible
		showGameOverText(true);     
	}
}