﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
	//The laser we'll be shooting
	public Transform laserPrefab;

	//Laser origin - distance from center of ship 
	public float laserDistance = 0.2f;

	//Time (in seconds) before we can fire again. 
	public float timeBetweenFires = 0.3f;

	//Time (in seconds) before we can fire again (counts down to zero). 
	private float timeTilNextFire = 0.0f;

	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
		if(Input.GetButton("Fire1") && timeTilNextFire < 0)
		{
			//Reset the timer and fire. 
			timeTilNextFire = timeBetweenFires;
			ShootLaser();
		}

		//Timer counts down based on deltaTime
		//time since last fire.  
		timeTilNextFire -= Time.deltaTime;
	}

	void ShootLaser()
	{
		//The laser originates from the players position 
		//so start with that. 
		Vector3 laserPos = this.transform.position;

		//Because of the direction the ship image in our
		//sprite is facing we need an angular offset to 
		//make sure it comes from the nose of the ship. 
		float rotationAngle = transform.localEulerAngles.z - 90;

		//Calculate the position infront of the ship and 
		//laser distance units away. 
		laserPos.x += (Mathf.Cos((rotationAngle) * Mathf.Deg2Rad) * -laserDistance);
		laserPos.y += (Mathf.Sin((rotationAngle) * Mathf.Deg2Rad) * -laserDistance);

		Instantiate(laserPrefab, laserPos, this.transform.rotation);

	}
}
