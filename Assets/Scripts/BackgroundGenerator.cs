﻿using UnityEngine;
using System.Collections;
using System;

public class BackgroundGenerator : MonoBehaviour
{
	/// <summary>
	/// Tile prefab to fill background.
	/// </summary>
	[SerializeField]
	public GameObject tilePrefab;

	/// <summary>
	/// Use this for initialization 
	/// </summary>
	void Start()
	{
        //Check we have a prefab for our tile 
		if (tilePrefab.GetComponent<Renderer> () == null) 
        {
			Debug.LogError ("There is no renderer available to fill background.");
		} 
        else 
        {
            // Find out the tile size
			Vector2 tileSize = getTileSize ();

            // Find out the size of the screen
			Vector2 screenQuarter = getScreenQuarterSize ();

            // Calculate the number of rows and 
            // columns which will fit on screen.

			//Ceil is a rounding method (Ceiling)
			int columns = Mathf.CeilToInt (screenQuarter.x / tileSize.x);
			int rows = Mathf.CeilToInt (screenQuarter.y / tileSize.y);

			createTiles (rows, columns, tileSize);
		}
	}

	Vector2 getTileSize()
	{
		//Get the sprite renderer attached to the tile
		Renderer rend = tilePrefab.GetComponent<Renderer>();

		//Get the size of its bounds component 
		Vector2 size = rend.bounds.size;

		//return this to the calling method.
		return size;
	}

	Vector2 getScreenQuarterSize()
	{
		// We need to work out how big the screen is from the camera size. 
		Camera mainCamera = Camera.main;

		Vector2 quarterOfScreen;

		//orthographic size = 1/2 the vertical viewing volume
		quarterOfScreen.y = mainCamera.orthographicSize * 2;

		//aspect (aspect ratio) = width / height
		//so width = aspect * height
		quarterOfScreen.x = mainCamera.aspect * quarterOfScreen.y;

		return quarterOfScreen;
	}

	void createTiles(int rows, int columns, Vector2 tileSize)
	{
		// from screen left side to screen right side, because camera is orthographic.
		for (int c = -columns; c < columns; c++) 
		{
			for (int r = -rows; r < rows; r++) 
			{
                //Calculate the position of the cetner of each tile. 
				//Note: + tileSize.x / 2 - position is the center of the image
				Vector2 position = new Vector2 (c * tileSize.x + tileSize.x / 2, r * tileSize.y + tileSize.y / 2);

                // Create the tiles
                // Position them 

				GameObject tile = Instantiate (tilePrefab, position, Quaternion.identity) as GameObject;
				tile.transform.parent = transform;
                // Add them to the scene.
			}
		}
	}
		
}