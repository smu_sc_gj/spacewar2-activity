<!-- 
Version 0.1 - Just starting this, I'll remove some bits and put them in a file so I can ensure I get it right on the day!  Need to get some images of the game running and add them to this repository, could also do with testing in the lab on Friday.
-->

# README #

Activity for the Cardiff Metropolian University Applicant Day. This is loosely based on Doran (2016), with some significant modifications.

Doran, J. P. (2016). Unity 5.x Game Development Blueprints, 2D Twin-stick Shooter, pp 1–50. Packt, Birmingham.

## Adding AI to the NPC ##

The task here is to add 'AI' to the spherical NPC (see image above) so that he attacks the player. 


<!-- Solution 

// Update is called once per frame
void Update()
{
	//Check the player attribute was set in Start().
	if (player == null)
	{
	    Debug.Log("Cannot find player ship, please " +
	    	      "check its called \"PlayerShip\"");
	}
	else
	{
	    //Get the distnance the players position to ours. 
	    Vector3 vectorToPlayer = player.position - transform.position;

	    //We want the direction only, not the magnitude
			vectorToPlayer.Normalize();

	    //distance to move, scaled based on time since last update. 
	    float dist = speed * Time.deltaTime;

	    //Move the enemy
            Vector3 movement = (vectorToPlayer * dist);
            transform.position = transform.position + movement;
	}
}
-->

## References ##

Doran, J. P. (2016). Unity 5.x Game Development Blueprints, 2D Twin-stick Shooter, pp 1–50. Packt, Birmingham.

## Credits ##

Resources from Doran (2016), available [here](https://github.com/PacktPublishing/Unity-5.x-Game-Development-Blueprints)

